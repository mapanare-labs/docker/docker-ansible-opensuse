FROM opensuse/leap:15.6
LABEL maintainer="Enmanuel Moreira"
ENV container=docker

ENV pip_packages "ansible github3.py"

# Enable systemd.
RUN zypper refresh \ 
    && zypper update -y \
    && zypper -n install systemd && \
    (cd /usr/lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == systemd-tmpfiles-setup.service ] || rm -f $i; done); \
    rm -f /usr/lib/systemd/system/multi-user.target.wants/*;\
    rm -f /etc/systemd/system/*.wants/*;\
    rm -f /usr/lib/systemd/system/local-fs.target.wants/*; \
    rm -f /usr/lib/systemd/system/sockets.target.wants/*udev*; \
    rm -f /usr/lib/systemd/system/sockets.target.wants/*initctl*; \
    rm -f /usr/lib/systemd/system/basic.target.wants/*;\
    rm -f /usr/lib/systemd/system/anaconda.target.wants/*;

# Install pip and other requirements.
RUN zypper in -y \
    python311-pip \
    sudo \
    which \
    python311-cryptography \
    system-group-wheel \
    && zypper clean --all \
    && rm -rf /var/log/* /var/run/log/journal \
    && rm -Rf /usr/share/doc \
    && rm -Rf /usr/share/man

# Install Ansible via Pip.
RUN pip3 install --no-cache-dir $pip_packages

# Disable requiretty.
RUN sed -i -e 's/^\(Defaults\s*requiretty\)/#--- \1/'  /etc/sudoers

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo -e '[local]\nlocalhost ansible_connection=local' > /etc/ansible/hosts

# Create `ansible` user with sudo permissions and membership in `DEPLOY_GROUP`
# This template gets rendered using `loop: "{{ molecule_yml.platforms }}"`, so
# each `item` is an element of platforms list from the molecule.yml file for this scenario.
ENV ANSIBLE_USER=ansible DEPLOY_GROUP=deployer
ENV SUDO_GROUP=wheel
RUN set -xe \
    && /usr/sbin/groupadd -r ${ANSIBLE_USER} \
    && /usr/sbin/groupadd -r ${DEPLOY_GROUP} \
    && /usr/sbin/useradd -m -g ${ANSIBLE_USER} ${ANSIBLE_USER} \
    && /usr/sbin/usermod -aG ${SUDO_GROUP} ${ANSIBLE_USER} \
    && /usr/sbin/usermod -aG ${DEPLOY_GROUP} ${ANSIBLE_USER} \
    && sed -i "/^%${SUDO_GROUP}/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers

VOLUME [ "/sys/fs/cgroup" ]
CMD ["/usr/lib/systemd/systemd", "--system"]